import React from 'react';
import './styles.css';

function Demo () {
    return (
        <div id="demo-content">
            <h2>How it's work ?</h2>
            <div className="iconExplain">
                <div className="start">
                    <div className="start-demo">
                        <img className="start-img" src="/start.png" alt="Start" />
                    </div>
                    <div className="start-explain">
                        <img src="/equalizer.svg" alt="parameters" width="100px" />
                        <h3>Edit your profil</h3>
                        <p className="text">Once connected, all you have to do is enter the sport you want to practice, your level 
                            of play and the place where you want to play.</p>
                    </div>
                </div>
                <div className="place">
                    <div className="place-explain">
                        <img src="/placeholder.svg" alt="place" width="100px" />
                        <h3>Find the place</h3>
                        <p className="text">Dose offers you all the places where you can practice your activity, indicating the 
                            frequency of places according to the schedules.</p>
                    </div>
                    <div className="place-demo">
                            <img className="place-img" src="/place.png" alt="Place" />
                    </div>
                </div>
                <div className="team">
                    <div className="team-demo">
                        <img className="team-img" src="/team.png" alt="Team" />
                    </div>
                    <div className="team-explain">
                        <img src="/group.svg" alt="team" width="100px" />
                        <h3>Join or create a team</h3>
                        <p className="text">Join and create teams quickly. With the level indicator you can form a team that you invite according 
                            to your objectives. A chat also allows you to talk to them.</p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Demo;