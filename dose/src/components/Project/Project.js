import React from 'react';
import './styles.css';

function Project() {
    return (
        <div id="Project-content">
            <img className="mockup" src="/mockups.png" alt="Mockups" />
            <div className="explication">
                <h1>The easiest way to find the best team mates, do sport where you want nearby !</h1>
                <p>DOSE is intended for athletes who want to quickly 
                    find players of their level around them. Once the team is complete, DOSE offers you a place around 
                    your home where you can practice your activity, 
                    indicating the number of people depending on the schedule.
                </p>
                <p>
                    
                </p>
                <a href="#form-content"><button className="button">
                    Be one of 100 first
                </button></a>
            </div>
        </div>
    );
}

export default Project;