import React from 'react';
import { Demo, Project, Story, Form, Price } from '../../components';
import { Footer } from "../Footer";

import './styles.css';

class LandingPage extends React.Component {
    render ()    {
        return (
                <div className="landing-wrapper">
                    <div className="content-wrapper">
                        <div id="project" />
                        <Project/>
                        <div id="features" />
                        <Demo/>
                        <div id="stories" />
                        <Price/>
                        <Story/>
                        <Form/>
                        <Footer/>
                    </div>
                </div>
        );
    }
}

export default LandingPage;