import React from 'react';
import './styles.css';

function Footer() {
    return(
        <div>
            <footer className="footer-content">
                <div className="footerLogo">
                    <a href="#project"><img id="photo" src="/logoS.png" alt="logo"/></a>
                </div>
                <small id="copy">&copy; Copyright 2021 DOSE Corporation</small>
            </footer>
        </div>
    );
}

export default Footer;